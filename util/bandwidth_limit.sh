#! /bin/bash

# limit bandwith of the interface
# ethtool/miitool do not work on VM, so we use tc htb + iptables instead

# Reference:
#  http://luxik.cdi.cz/~devik/qos/htb/manual/userg.htm#sharing
#  http://serverfault.com/questions/191560/how-can-i-do-traffic-shaping-in-linux-by-ip

#set -x

NETCARD=$1
MAXBANDWIDTH=1000

# Flush
iptables -t mangle -F

# reinit
tc qdisc del dev $NETCARD root handle 1
tc qdisc add dev $NETCARD root handle 1: htb default 9999

# create the default class
tc class add dev $NETCARD parent 1:0 classid 1:9999 htb rate $(( $MAXBANDWIDTH ))kbit ceil $(( $MAXBANDWIDTH ))kbit burst 5k prio 9999

#propagate netfilter marks on connections
iptables -t mangle -A POSTROUTING -j CONNMARK --restore-mark
